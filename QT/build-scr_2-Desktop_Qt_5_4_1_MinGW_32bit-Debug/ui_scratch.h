/********************************************************************************
** Form generated from reading UI file 'scratch.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCRATCH_H
#define UI_SCRATCH_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Scratch
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Scratch)
    {
        if (Scratch->objectName().isEmpty())
            Scratch->setObjectName(QStringLiteral("Scratch"));
        Scratch->resize(400, 300);
        menuBar = new QMenuBar(Scratch);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        Scratch->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Scratch);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        Scratch->addToolBar(mainToolBar);
        centralWidget = new QWidget(Scratch);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        Scratch->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(Scratch);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        Scratch->setStatusBar(statusBar);

        retranslateUi(Scratch);

        QMetaObject::connectSlotsByName(Scratch);
    } // setupUi

    void retranslateUi(QMainWindow *Scratch)
    {
        Scratch->setWindowTitle(QApplication::translate("Scratch", "Scratch", 0));
    } // retranslateUi

};

namespace Ui {
    class Scratch: public Ui_Scratch {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCRATCH_H
