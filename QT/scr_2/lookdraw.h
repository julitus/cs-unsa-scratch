#ifndef LOOKDRAW_H
#define LOOKDRAW_H

#include "blockmain.h"

class LookDraw : public BlockMain{

public:
    LookDraw(Action *action, QWidget *parent = 0);
    void eventAction();
    LookDraw *copy(QWidget *parent = 0);
    virtual ~LookDraw();

};

#endif // LOOKDRAW_H
