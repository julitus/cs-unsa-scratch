#ifndef LOOKSAY_H
#define LOOKSAY_H

#include "blockmain.h"

class LookSay : public BlockMain{

public:
    LookSay(Action *action, QWidget *parent = 0);
    void eventAction();
    LookSay *copy(QWidget *parent = 0);
    virtual ~LookSay();

};

#endif // LOOKSAY_H
