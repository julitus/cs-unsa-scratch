#ifndef MOVEGOTOY_H
#define MOVEGOTOY_H

#include "blockmain.h"

class MoveGoToY : public BlockMain{

public:
    MoveGoToY(Action *action, QWidget *parent = 0);
    void eventAction();
    MoveGoToY *copy(QWidget *parent = 0);
    virtual ~MoveGoToY();

};

#endif // MOVEGOTOY_H
