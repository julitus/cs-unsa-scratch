/**
   *@file  action.h
   * @title Clase de acciones
    */
#ifndef ACTION_H
#define ACTION_H

#include "newtype.h"
#include "cat.h"
#include "position.h"
#include <list>


/**
 * @brief The Action class  Esta clase esta encargada de dar las acciones a los bloques e interactuara con el
personaje.
 */
class Action{

public:
    /**
     * @brief Action  Instancia un objeto de tipo 'Action' con los parametros 'cat' y 'position'
     * @param cat  'cat' es el personaje donde se aplicaran los metodos
     * @param position Con este parametro se actualizara la posicion del personaje
     */
    Action(Cat *cat, Position *position);
    /**
     * @brief getCat Funcion que retorna
     * @return  Retorna el puntero 'cat'
     */
    Cat *getCat();
    /**
     * @brief setCat Guarda el puntero 'cat' que es el personaje
     * @param cat  Aquel puntero es guaradado en esta variable 'cat'
     */
    void setCat(Cat *cat);
    /**
     * @brief moveStep  Con este metodo hacemos que el personaje se mueva
     * @param x Esta variable sera la cantidad de pasos que dara el personaje
     */
    void moveStep(conv x);
    /**
     * @brief turnLeft  Permite que el personaje pueda rotar hacia la izquierda
     * @param g Es la cantidad en grados que se va a rotar
     */
    void turnLeft(conv g);
    /**
     * @brief turnRight Permite que el personaje rote a la derecha
     * @param g Cantidad en grados que se va a rotar
     */
    void turnRight(conv g);
    /**
     * @brief editAngle  Permite cambiar el angulo de rotacion
     * @param g Aqui se almacena el valor
     */
    void editAngle(conv g);
    /**
     * @brief goTo_x  Indica  ir a la posicion X
     * @param x Guarda el valor de la posicion
     */
    void goTo_x(conv x);
    /**
     * @brief goTo_y Indica  ir a la posicion Y
     * @param y  Guarda el valor de la posicion
     */
    void goTo_y(conv y);
    /**
     * @brief sayWord Esta función es parar que aparesca el dialogo del personaje con una imagen de fondo
     * @param word Parametro que pasa el texto y es recibido por el metodo para ser mostrado en pantalla.
     */
    void sayWord(dataText word);
    /**
     * @brief thinkWord  Se muestra un dialogo pero este con una imagen de fondo de pensamiento.
     * @param word   Parametro que pasa el texto y es recibido por el metodo para ser mostrado en pantalla.
     */
    void thinkWord(dataText word);
    /**
     * @brief show Muestra el personaje en este ocacion es un gato.
     */
    void show();
    /**
     * @brief hide Oculta el personaje en este ocacion es un gato.
     */
    void hide();
    /**
     * @brief wait Metodo del tiempo
     * @param sec
     */
    void wait(PosE sec);
    void delay(PosE time);
    /**
     * @brief drawLine Este metodo esta encargado de dibujar el recorrido que haga el personaje.
     */
    void drawLine();
    /**
     * @brief removeLine
     */
    void removeLine();
    void increaseStep(conv x);
    void drawChange(bool change);
    void defaultParams();
    /**
     * @brief ~Action Eliminamos el objeto instanciado
     */
    virtual ~Action();

private:
    Cat *cat;
    Position *position;
    conv upStep;
    std:: list<QGraphicsLineItem *> lines;
    bool draw;
    void createMsj(dataText path, dataText word, conv x, conv y);

};

#endif // ACTION_H
