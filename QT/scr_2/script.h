#ifndef SCRIPT_H
#define SCRIPT_H

#include "drags.h"
#include "action.h"

//clase Padre encargado de arrastrar bloques hacia Run
//y eliminar bloques que vienen de Run
class Script : public Drags{

public:
    Script(Action *action, QWidget *parent = 0);
    void setAction(Action *action);
    Action *getAction();
    virtual ~Script();

private:
    Action *action;

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dropEvent(QDropEvent *event);
    void mousePressEvent(QMouseEvent *event);

};

#endif // SCRIPT_H
