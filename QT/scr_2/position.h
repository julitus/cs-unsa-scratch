#ifndef POSITION_H
#define POSITION_H

#include <QGraphicsItem>
#include <QGraphicsTextItem>
#include "newtype.h"
#include "cat.h"

//clase encargada de mostrar la posicion de Cat
class Position : public QGraphicsTextItem{

public:
    Position(Cat *cat, QGraphicsItem *parent = 0);
    virtual ~Position();
    void showPosition();

private:
    Cat *cat;

};

#endif // POSITION_H
