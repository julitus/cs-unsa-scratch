#include "lookwait.h"
#include "textvalue.h"

LookWait::LookWait(Action *action, QWidget *parent): BlockMain(action, parent){

    //Ingresamos la imagen del bloque y la posicion
    this->type = MOTI;
    this->name = "lookwait";
    this->setPixmap(QPixmap(":/img/wait.png"));

    //Instanciamos el campo de texto y lo agregamos al bloque
    TextValue *text = new TextValue(this);
    text->move(60, 3);
    text->setFixedSize(25, 14);
    text->setText("1");
    this->setBuddy(text);

    //mostramos el bloque
    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);
}

void LookWait::eventAction(){

    this->getAction()->wait(static_cast<TextValue*>(this->buddy())->toPlainText().toDouble());

}

LookWait *LookWait::copy(QWidget *parent){

    return new LookWait(this->getAction(), parent);

}

LookWait::~LookWait(){

}

