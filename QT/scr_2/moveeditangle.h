#ifndef MOVEEDITANGLE_H
#define MOVEEDITANGLE_H

#include "blockmain.h"

class MoveEditAngle : public BlockMain{

public:
    MoveEditAngle(Action *action, QWidget *parent = 0);
    virtual ~MoveEditAngle();
    void eventAction();
    MoveEditAngle *copy(QWidget *parent = 0);

};

#endif // MOVEEDITANGLE_H
