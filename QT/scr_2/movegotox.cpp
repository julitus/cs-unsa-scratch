#include "movegotox.h"
#include "textvalue.h"

MoveGoToX::MoveGoToX(Action *action, QWidget *parent): BlockMain(action, parent){

    //Ingresamos la imagen del bloque y la posicion
    this->type = MOTI;
    this->name = "movegotox";
    this->setPixmap(QPixmap(":/img/gotox.png"));

    //Instanciamos el campo de texto y lo agregamos al bloque
    TextValue *text = new TextValue(this);
    text->move(60, 3);
    text->setText("0");
    this->setBuddy(text);

    //mostramos el bloque
    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);

}

MoveGoToX::~MoveGoToX(){

}

void MoveGoToX::eventAction(){

    this->getAction()->goTo_x(static_cast<TextValue*>(this->buddy())->toPlainText().toDouble());

}

MoveGoToX *MoveGoToX::copy(QWidget *parent){

    return new MoveGoToX(this->getAction(), parent);

}
