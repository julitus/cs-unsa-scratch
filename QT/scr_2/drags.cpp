#include "drags.h"                       //clase padre de los contenedores pero solo para run y events
#include <QtWidgets>

Drags::Drags(QWidget *parent): QFrame(parent){

    //Variable que guarda la posicion en la lista de Widgets de la clase
    this->elemClick = -1;

}

Drags::~Drags(){

}

void Drags::setElemClick(PosE pos){

    this->elemClick = pos;

}

PosE Drags::getElemClick(){

    return this->elemClick;

}

//evento de inicio del Drag(arrastrar)
void Drags::dragEnterEvent(QDragEnterEvent *event){

    //validamos el evento segun sea el caso
    if (event->mimeData()->hasFormat("application/x-dnditemdata")) {
        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } else {
            event->acceptProposedAction();
        }
    } else {
        event->ignore();
    }

}

//evento de movimiento del Drag
void Drags::dragMoveEvent(QDragMoveEvent *event){

    //validamos el evento segun sea el caso
    if (event->mimeData()->hasFormat("application/x-dnditemdata")) {
        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } else {
            event->acceptProposedAction();
        }
    } else {
        event->ignore();
    }

}

//evento de Drop(soltar)
void Drags::dropEvent(QDropEvent *event){

    if (event->mimeData()->hasFormat("application/x-dnditemdata")) {

        //extraemos la informacion y el mapa de pixeles
        //y lo desempaquetamos con el QDataStream
        QByteArray itemData = event->mimeData()->data("application/x-dnditemdata");
        QDataStream dataStream(&itemData, QIODevice::ReadOnly);

        QPixmap pixmap;
        QPoint offset;
        dataStream >> pixmap >> offset;

        //Instanciamos un nuevo QLabel
        QLabel *newIcon = new QLabel(this);
        newIcon->setPixmap(pixmap);
        newIcon->move(event->pos() - offset);
        newIcon->show();
        newIcon->setAttribute(Qt::WA_DeleteOnClose);

        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } else {
            event->acceptProposedAction();
        }
    } else {
        event->ignore();
    }

}

//evento del click
void Drags::mousePressEvent(QMouseEvent *event){

    //Aseguramos que el click se haga sobre un QLabel
    QLabel *child = static_cast<QLabel*>(childAt(event->pos()));
    if (!child){
        return;
    }

    Str name(childAt(event->pos())->metaObject()->className());
    if(name == "QTextEdit"){
        return;
    }

    QPixmap pixmap = *child->pixmap();

    //enviamos la informacion y el mapa de pixeles
    //y lo empaquetamos con el QDataStream
    QByteArray itemData;
    QDataStream dataStream(&itemData, QIODevice::WriteOnly);
    dataStream << pixmap << QPoint(event->pos() - child->pos());

    QMimeData *mimeData = new QMimeData;
    mimeData->setData("application/x-dnditemdata", itemData);

    //Instanciamos un objecto QDrag para la accion de soltar y arrastrar
    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeData);
    drag->setPixmap(pixmap);
    drag->setHotSpot(event->pos() - child->pos());

    //Creamos una copia del mapa de pixeles
    QPixmap tempPixmap = pixmap;
    QPainter painter;
    painter.begin(&tempPixmap);
    painter.fillRect(pixmap.rect(), QColor(colorFOND, colorFOND, colorFOND, colorFOND));
    painter.end();

    child->setPixmap(tempPixmap);

    //validamos el evento
    if (drag->exec(Qt::CopyAction | Qt::MoveAction, Qt::CopyAction) == Qt::MoveAction) {
        child->close();
    } else {
        child->show();
        child->setPixmap(pixmap);
    }

}
