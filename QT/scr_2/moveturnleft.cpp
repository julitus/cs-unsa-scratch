#include "moveturnleft.h"
#include "textvalue.h"

MoveTurnLeft::MoveTurnLeft(Action *action, QWidget *parent): BlockMain(action, parent){

    this->type = MOTI;
    this->name = "moveturnleft";
    this->setPixmap(QPixmap(":/img/turnleft.png"));

    TextValue *text = new TextValue(this);
    text->move(60, 3);
    text->setText("15");
    this->setBuddy(text);

    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);

}

MoveTurnLeft::~MoveTurnLeft(){

}

void MoveTurnLeft::eventAction(){

    this->getAction()->turnLeft(static_cast<TextValue*>(this->buddy())->toPlainText().toDouble());
}

MoveTurnLeft *MoveTurnLeft::copy(QWidget *parent){

    return new MoveTurnLeft(this->getAction(), parent);

}
