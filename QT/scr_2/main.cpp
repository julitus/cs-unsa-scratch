#include "creator.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(scratch);

    QApplication a(argc, argv);

    //Creamos las ventana y la mostramos
    Creator *mainWid = new Creator();
    mainWid->show();

    return a.exec();
}
