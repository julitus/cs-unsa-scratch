#include "movegotoy.h"
#include "textvalue.h"

MoveGoToY::MoveGoToY(Action *action, QWidget *parent): BlockMain(action, parent){

    //Ingresamos la imagen del bloque y la posicion
    this->type = MOTI;
    this->name = "movegotoy";
    this->setPixmap(QPixmap(":/img/gotoy.png"));

    //Instanciamos el campo de texto y lo agregamos al bloque
    TextValue *text = new TextValue(this);
    text->move(60, 3);
    text->setText("0");
    this->setBuddy(text);

    //mostramos el bloque
    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);

}

MoveGoToY::~MoveGoToY(){

}

void MoveGoToY::eventAction(){

    this->getAction()->goTo_y(static_cast<TextValue*>(this->buddy())->toPlainText().toDouble());

}

MoveGoToY *MoveGoToY::copy(QWidget *parent){

    return new MoveGoToY(this->getAction(), parent);

}
