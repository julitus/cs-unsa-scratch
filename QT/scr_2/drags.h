#ifndef DRAGS_H
#define DRAGS_H

#include <QFrame>
#include "newtype.h"

//clase Padre encargado del Drag and Drop
class Drags: public QFrame{

public:
    Drags(QWidget *parent = 0);
    void setElemClick(PosE pos);
    PosE getElemClick();
    virtual ~Drags();

private:
    PosE elemClick;

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dropEvent(QDropEvent *event);
    void mousePressEvent(QMouseEvent *event);

};

#endif // DRAGS_H
