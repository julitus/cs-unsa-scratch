#include "looksay.h"
#include "textvalue.h"

LookSay::LookSay(Action *action, QWidget *parent): BlockMain(action, parent){

    //Ingresamos la imagen del bloque y la posicion
    this->type = MOTI;
    this->name = "looksay";
    this->setPixmap(QPixmap(":/img/looksay.png"));

    //Instanciamos el campo de texto y lo agregamos al bloque
    TextValue *text = new TextValue(this);
    text->move(60, 3);
    text->setFixedSize(70, 14);
    text->setText("Hola!");
    this->setBuddy(text);

    //mostramos el bloque
    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);
}

void LookSay::eventAction(){

    this->getAction()->sayWord(static_cast<TextValue*>(this->buddy())->toPlainText());

}

LookSay *LookSay::copy(QWidget *parent){

    return new LookSay(this->getAction(), parent);

}

LookSay::~LookSay(){

}

