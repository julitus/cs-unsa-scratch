#ifndef MOVETURNLEFT_H
#define MOVETURNLEFT_H

#include "blockmain.h"

class MoveTurnLeft : public BlockMain{

public:
    MoveTurnLeft(Action *action, QWidget *parent = 0);
    void eventAction();
    MoveTurnLeft *copy(QWidget *parent = 0);
    virtual ~MoveTurnLeft();

};

#endif // MOVETURNLEFT_H
