#include "position.h"

Position::Position(Cat *cat, QGraphicsItem *parent): QGraphicsTextItem(parent){

    //asignamos la posicion en la escena y otros valores
    this->cat = cat;
    this->setPos(0, 0);
    this->setDefaultTextColor(Qt::blue);
    this->setFont(QFont("verdana",sizeWORD));

}

Position::~Position(){

}

void Position::showPosition(){

    conv x = this->cat->x() + this->cat->getWidth()/2;
    conv y = -(this->cat->y() + this->cat->getHeight()/2);
    this->setPlainText(QString("x:") + QString::number(x) +
                       QString(" y:") + QString::number(y));
    this->cat->setXAns(x);
    this->cat->setYAns(-y);

}

