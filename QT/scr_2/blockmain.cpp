#include "blockmain.h"
#include "textvalue.h"
#include "run.h"
#include <typeinfo>

BlockMain::BlockMain(){

}

BlockMain::BlockMain(Action *action, QWidget *parent): QLabel (parent){

    this->action = action;

}

BlockMain::~BlockMain(){

}

dataText BlockMain::getName(){

    return this->name;

}

typeBlock BlockMain::getType(){

    return this->type;

}

void BlockMain::setAction(Action *action){

    this->action = action;

}

Action *BlockMain::getAction(){

    return this->action;

}

QMap<PosE, BlockMain *> *BlockMain::getListMap(){

    return this->listMap;

}

void BlockMain::setListMap(QMap<PosE, BlockMain *> &listMap){

    this->listMap = &listMap;

}

//metodo que ejecuta la accion del bloque con doble click
void BlockMain::mouseDoubleClickEvent(QMouseEvent *event){

    if(typeid(*this->parent()) != typeid(Run) or this->getType()){
        this->eventAction();
        QLabel::mouseDoubleClickEvent(event);
    }

}
