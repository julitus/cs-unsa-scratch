/**
*@file cat.h
* @name clase cat o del personaje
*/
#ifndef CAT_H
#define CAT_H

#include <QGraphicsPixmapItem>
#include "newtype.h"
/**
 * @brief The Cat class Esta clase esta hecha para el personaje con el que se va a interactuar
 */

class Cat : public QGraphicsPixmapItem{

public:
    /**
     * @brief Cat
     * @param parent
     */
    Cat(QGraphicsItem *parent = 0);
    /**
     * @brief ~Cat
     */
    virtual ~Cat();
    /**
     * @brief getWidth
     * @return
     */
    PosE getWidth();
    /**
     * @brief getHeight
     * @return
     */
    PosE getHeight();
    /**
     * @brief setMsj
     * @param value
     */
    conv getXAns();
    void setXAns(conv value);
    conv getYAns();
    void setYAns(conv value);

private:
    PosE width; /** Variable que alamcenara el ancho de la imagen del personaje*/
    PosE height;/** Variable que alamcenara la alturade la imagen del personaje */
    conv xAns;
    conv yAns;

};

#endif // CAT_H
