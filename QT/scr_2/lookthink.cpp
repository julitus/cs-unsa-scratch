#include "lookthink.h"
#include "textvalue.h"

LookThink::LookThink(Action *action, QWidget *parent): BlockMain(action, parent){

    //Ingresamos la imagen del bloque y la posicion
    this->type = MOTI;
    this->name = "lookthink";
    this->setPixmap(QPixmap(":/img/lookthink.png"));

    //Instanciamos el campo de texto y lo agregamos al bloque
    TextValue *text = new TextValue(this);
    text->move(60, 3);
    text->setFixedSize(70, 14);
    text->setText("Ayayay...");
    this->setBuddy(text);

    //mostramos el bloque
    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);
}

void LookThink::eventAction(){

    this->getAction()->thinkWord(static_cast<TextValue*>(this->buddy())->toPlainText());

}

LookThink *LookThink::copy(QWidget *parent){

    return new LookThink(this->getAction(), parent);

}

LookThink::~LookThink(){

}

