#ifndef LOOKHIDE_H
#define LOOKHIDE_H

#include "blockmain.h"

class LookHide : public BlockMain{

public:
    LookHide(Action *action, QWidget *parent = 0);
    void eventAction();
    LookHide *copy(QWidget *parent = 0);
    virtual ~LookHide();

};

#endif // LOOKHIDE_H
