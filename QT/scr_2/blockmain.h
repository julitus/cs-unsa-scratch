/**
  *@file BlockMain.h}
  * @name Clase Padre de los bloques
  */

#ifndef BLOCKMAIN_H
#define BLOCKMAIN_H

#include <QLabel>
#include "action.h"

/**
 * @brief The BlockMain class Esta es la clase Padre encargada de todos los bloques de movimiento, ejecucion y control. Además esta clase hace herencia
 * de la clase QLabel que es propia del programa QT
 */
class BlockMain: public QLabel{

public:
    BlockMain();
    /**
     * @brief BlockMain  Este constructor recibe un parametro 'action' y un QWidget que hace referencia a su padre.
     * @param action Parametro que sera manipulado en cada clase hija del blockmain.
     * @param parent
     */
    BlockMain(Action* action, QWidget *parent = 0);

    /**
     * @brief eventAction Metodo virtual que ejecuta la accion del bloque.
     */
    virtual void eventAction() = 0;
    //metodo que crea una nueva instancia del bloque
    /**
     * @brief copy Metodo virtual de tipo BlockMain que se implementa en cada clase hija para generar un nuevo bloque u objeto.
     * @param parent  Hace referencia a la clases padre
     * @return
     */
    virtual BlockMain* copy(QWidget *parent = 0) = 0;
    /**
     * @brief getType Para obtener el tipo del bloque.
     * @return  Aqui se retorna el tipo.
     */
    typeBlock getType() ;
    /**
     * @brief setAction Guardar la acción.
     * @param action Parametro de accion que ingresa.
     */
    void setAction(Action *action);
    /**
     * @brief getAction
     * @return
     */
    Action *getAction();
    /**
     * @brief getListMap
     * @return
     */
    QMap<PosE, BlockMain *> *getListMap();
    /**
     * @brief setListMap
     * @param listMap
     */
    void setListMap(QMap<PosE, BlockMain *> &listMap);
    /**  @brief ~BlockMain        */
    dataText getName();
    virtual ~BlockMain();

private:
    /** @brief action    */
    Action *action;
    /**  @brief listMap   */
    QMap<PosE, BlockMain *> *listMap;

protected:
    /**  @brief type     */
    typeBlock type;
    /**  @brief name     */
    dataText name;
    /**
     * @brief mouseDoubleClickEvent
     * @param event
     */
    void mouseDoubleClickEvent(QMouseEvent *event);

};

#endif // BLOCKMAIN_H
