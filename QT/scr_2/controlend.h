/**
  *@file controlend.h
  * @name Bloque end
  */
#ifndef CONTROLEND_H
#define CONTROLEND_H

#include "blockmain.h"
/**
 * @brief The ControlEnd class
 */
class ControlEnd : public BlockMain{

public:
    /**
     * @brief ControlEnd
     * @param action
     * @param parent
     */
    ControlEnd(Action *action, QWidget *parent = 0);
    /**
     * @brief ~ControlEnd
     */
    virtual ~ControlEnd();
    /**
     * @brief eventAction
     */
    void eventAction();
    /**
      * @brief copy
      * @param parent
      * @return
      */
     ControlEnd *copy(QWidget *parent = 0);

};

#endif // CONTROLEND_H
