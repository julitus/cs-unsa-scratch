#include "moveturnright.h"
#include "textvalue.h"

MoveTurnRight::MoveTurnRight(Action *action, QWidget *parent): BlockMain(action, parent){

    this->type = MOTI;
    this->name = "moveturnright";
    this->setPixmap(QPixmap(":/img/turnright.png"));

    TextValue *text = new TextValue(this);
    text->move(60, 3);
    text->setText("15");
    this->setBuddy(text);

    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);

}

void MoveTurnRight::eventAction(){

    this->getAction()->turnRight(static_cast<TextValue*>(this->buddy())->toPlainText().toDouble());

}

MoveTurnRight *MoveTurnRight::copy(QWidget *parent){

    return new MoveTurnRight(this->getAction(), parent);

}

MoveTurnRight::~MoveTurnRight(){

}

