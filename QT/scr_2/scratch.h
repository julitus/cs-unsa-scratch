#ifndef SCRATCH_H
#define SCRATCH_H

#include <QFrame>
#include "view.h"
#include "exestart.h"

class Scratch : public QFrame{

public:
    Scratch(Cat *cat, QWidget *parent = 0);
    void setListMap(QMap<PosE, BlockMain *> &listMap);
    virtual ~Scratch();
    Position *getPosition();
    void setAction(Action *action);

private:
    View *barPos;
    ExeStart *start;
    View *view;

};

#endif // SCRATCH_H
