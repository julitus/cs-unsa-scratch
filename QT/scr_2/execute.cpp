#include "execute.h"

Execute::Execute(Action *action, QWidget *parent): Script(action, parent){

    //Tamaño minimo del contenedor
    this->setMinimumSize(200, 400);
    this->setAcceptDrops(true);

    //Instanciamos los bloques de movimiento
    this->start = new ExeStart(this->getAction(), this);
    this->start->move(xBLOCK, yBLOCK);

}

void Execute::setListMap(QMap<PosE, BlockMain *> &listMap){

    this->start->setListMap(listMap);

}

Execute::~Execute(){

}
