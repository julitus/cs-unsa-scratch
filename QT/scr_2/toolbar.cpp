#include "toolbar.h"
#include <iostream>
#include <fstream>
#include <QtWidgets>
#include "textvalue.h"

ToolBar::ToolBar(Run *run, Action * action){

    this->run = run;
    this->action = action;

    //creamos las acciones y las conectamos con un metodo de la clase
    QAction *openAct = new QAction(QIcon(":/img/open.png"), tr("&Open..."), this);
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    QAction *saveAct = new QAction(QIcon(":/img/save.png"), tr("&Save"), this);
    connect(saveAct, SIGNAL(triggered()), this, SLOT(saveAs()));

    QAction *cleanAct = new QAction(QIcon(":/img/clean.png"), tr("&Clean"), this);
    connect(cleanAct, SIGNAL(triggered()), this, SLOT(clean()));

    //agregamos las acciones a la barra
    this->addAction(openAct);
    this->addAction(saveAct);
    this->addAction(cleanAct);

    //creamos el mapa de punteros a metodos que crearan los BlockMain
    this->createMap();

}

void ToolBar::open(){

    //creamos la ventana de Abrir
    dataText fileName = QFileDialog::getOpenFileName(this, "Abrir", "../scr_2/test", tr("Text Files(*.txt)"));
    if (!fileName.isEmpty()){
        //mandamos la ruta del archivo seleccionado
        loadFile(fileName);
    }

}

bool ToolBar::saveAs(){

    //creamos la ventana de guardar como
    QFileDialog dialog(this, "Guardar como", "../scr_2/test", tr("Text Files(*.txt)"));
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dataText fileName;
    //ejecutamos la ventana
    if(dialog.exec()){
        //guardamos las rutas de los archivos seleccionados
        fileName = dialog.selectedFiles().at(0);
    }else{
        return false;
    }
    //mandamos la ruta del archivo
    return saveFile(fileName);

}

bool ToolBar::clean(){

    //creamos la ventana de advertencia
    QMessageBox::StandardButton msg;
    msg = QMessageBox::question(this, "Advertencia", "Limpiara los bloques en ejecucion ¿esta Ud. seguro?",
                                QMessageBox::Yes|QMessageBox::No);
    //comprobamos si fue aceptada la peticion
    if(msg == QMessageBox::Yes) {
        //limpiamos
        cleanAll();
        return true;
    }
    return false;

}

void ToolBar::addVector(std::vector<Str> &words, Str line){

    Str word = "";//para guardar una palabra
    std::for_each(line.begin(), line.end(), [&words, &word](ch &c){
        if(c == ' '){
            words.push_back(word); word = ""; return;
        }
        word.push_back(c);
    });
    words.push_back(word); //cuando termino la linea queda una plabra si guardar entonces aquila guarda

}

void ToolBar::loadFile(dataText fileName){

    //comprobamos si acepto la peticion de clean
    if(this->clean()){
        Str line;// se va a guardar laprimera linea del documento
        std::vector<Str> words;//las palabras que extraemos por linea
        std::ifstream myfile (fileName.toStdString());//maanipulamos el archivo con el 'filename' lo abre
        if (myfile.is_open()){//abre el archivo
            while ( getline (myfile, line) ){//por cada bucle va obteniedo cada linea

                /*-------IMPORTANT---------*/
                this->addVector(words, line);//guardamos las palabras de la linea en forma de vector

                //llamamos el puntero al metodo segun el primer elemento del vector y
                //ejecutamos el metodo devolviendo un BlockMain asignado al *block
                //los bloques se crean en Run por tenerlo como padre, esto es hecho por el constructor
                BlockMain *block = (this->*mapBlock.find(words[0])->second)();
                block->move(std::stoi(words[1]), std::stoi(words[2]));//asignamos la posicion en Run
                //comprobamos si el bloque tiene alguna caja de texto
                if(block->buddy()){
                    static_cast<TextValue*>(block->buddy())->setPlainText(dataText::fromStdString(words[3]));
                }
                //insertamos el bloque al mapa para que pueda ser ejecutado
                this->run->insertBlockMap(block);
                words.clear();// limpia el vector para guardar nuevos datos de la siguiente linea
                /*-------------------------*/

            }
            myfile.close();//cerramos el archivo
        }
    }

}

bool ToolBar::saveFile(dataText fileName){//el parametreo ingresa como qstring

    std::ofstream myfile;//objeto del odstream para manejar archivos
    myfile.open(fileName.toStdString());//si no existe el archivo lo crea en ese momento-----el toString convierte el file name en string

    std::for_each(this->run->getListMap()->begin(), this->run->getListMap()->end(), [&myfile](BlockMain *tb){
        myfile << tb->getName().toStdString() << " " << tb->x() << " " << tb->y();
        if(tb->buddy()){
            myfile << " " << static_cast<TextValue*>(tb->buddy())->toPlainText().toStdString() << "\n";
        }else{
            myfile << "\n";
        }
    });

    myfile.close();//aqui se cierra el archivo
    return true;
}

void ToolBar::cleanAll(){

    if(!this->run->getListMap()->isEmpty()){//comprobamos si no esta vacio nuestro mapa de 'run'

        foreach(BlockMain *tb, this->run->findChildren<BlockMain *>()){//buscar todos los hijos de tipo blockmain
            tb->~BlockMain();//cada uno se va a ir eliminando
        }
        this->run->getListMap()->clear();//limpiamos el mapa
    }

}

void ToolBar::createMap(){

    mapBlock["movestep"] = &ToolBar::createMoveStep;
    mapBlock["movegotox"] = &ToolBar::createMoveGoToX;
    mapBlock["movegotoy"] = &ToolBar::createMoveGoToY;
    mapBlock["moveturnleft"] = &ToolBar::createMoveTurnLeft;
    mapBlock["moveturnright"] = &ToolBar::createMoveTurnRight;
    mapBlock["moveincreasestep"] = &ToolBar::createMoveIncreaseStep;
    mapBlock["moveeditangle"] = &ToolBar::createMoveEditAngle;
    mapBlock["lookwait"] = &ToolBar::createLookWait;
    mapBlock["lookthink"] = &ToolBar::createLookThink;
    mapBlock["lookshow"] = &ToolBar::createLookShow;
    mapBlock["looksay"] = &ToolBar::createLookSay;
    mapBlock["lookhide"] = &ToolBar::createLookHide;
    mapBlock["lookdraw"] = &ToolBar::createLookDraw;
    mapBlock["exestart"] = &ToolBar::createExeStart;
    mapBlock["controlend"] = &ToolBar::createControlEnd;
    mapBlock["controlfor"] = &ToolBar::createControlFor;

}
