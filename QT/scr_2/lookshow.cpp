#include "lookshow.h"

LookShow::LookShow(Action *action, QWidget *parent): BlockMain(action, parent){

    //Ingresamos la imagen del bloque y la posicion
    this->type = MOTI;
    this->name = "lookshow";
    this->setPixmap(QPixmap(":/img/lookshow.png"));

    //mostramos el bloque
    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);
}

void LookShow::eventAction(){

    this->getAction()->show();

}

LookShow *LookShow::copy(QWidget *parent){

    return new LookShow(this->getAction(), parent);

}

LookShow::~LookShow(){

}

