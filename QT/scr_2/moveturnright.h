#ifndef MOVETURNRIGHT_H
#define MOVETURNRIGHT_H

#include "blockmain.h"

class MoveTurnRight : public BlockMain{

public:
    MoveTurnRight(Action *action, QWidget *parent = 0);
    void eventAction();
    MoveTurnRight *copy(QWidget *parent = 0);
    virtual ~MoveTurnRight();

};

#endif // MOVETURNRIGHT_H
