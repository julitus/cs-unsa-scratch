#include "events.h"
#include <QVBoxLayout>

Events::Events(Run *run, Action *action, QWidget *parent): QFrame(parent){

    //Ancho fijo, altura minima del bloque y estilo
    this->setFixedWidth(250);
    this->setMinimumHeight(400);
    this->setFrameStyle(QFrame::Sunken);
    this->setAcceptDrops(false);

    //Instanciamos las clases que seran las pestañas del bloque
    this->action = action;
    this->move = new Moves(this->action, this);
    this->control = new Controls(this->action, this);
    this->execute = new Execute(this->action, this);
    this->bar = new ToolBar(run, this->action);

    //Agregamos las instancias a un QTabWidget y lo mostramos
    QTabWidget *event = new QTabWidget(this);
    event->move(INI, INI);
    event->setMinimumSize(200, 400);
    event->addTab(move, "Motion");
    event->addTab(control, "Control");
    event->addTab(execute, "Event");
    event->show();


    //Agregamos el QTabWidget a un contenedor
    QVBoxLayout *contLayout = new QVBoxLayout(this);
    contLayout->setSpacing(INI);
    contLayout->setContentsMargins (INI, INI, INI, INI);
    contLayout->addWidget(this->bar);
    contLayout->addWidget(event);

    this->setLayout(contLayout);

}

void Events::setListMap(QMap<PosE, BlockMain *> &listMap){

    this->execute->setListMap(listMap);

}

Events::~Events(){

}
