#ifndef MOVES_H
#define MOVES_H

#include "script.h"
#include "movestep.h"
#include "moveturnleft.h"
#include "moveturnright.h"
#include "movegotox.h"
#include "movegotoy.h"
#include "moveeditangle.h"
#include "looksay.h"
#include "lookthink.h"
#include "lookshow.h"
#include "lookhide.h"
#include "lookdraw.h"
#include "moveincreasestep.h"

class Moves: public Script{

public:
    Moves(Action *action, QWidget *parent = 0);
    virtual ~Moves();

private:
    MoveStep *step;
    MoveTurnLeft *turnLeft;
    MoveTurnRight *turnRight;
    MoveGoToX *goToX;
    MoveGoToY *goToY;
    MoveEditAngle *editAngle;
    LookSay *say;
    LookThink *think;
    LookShow *show;
    LookHide *hide;
    LookDraw *draw;
    MoveIncreaseStep *increaseStep;

};

#endif // MOVES_H
