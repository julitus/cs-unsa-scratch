#include "script.h"
#include "run.h"
#include "blockmain.h"
#include <typeinfo>

Script::Script(Action *action, QWidget *parent): Drags(parent){

    this->action = action;

}

void Script::setAction(Action *action){

    this->action = action;

}

Action *Script::getAction(){

    return this->action;

}

Script::~Script(){

}

void Script::dragEnterEvent(QDragEnterEvent *event){

    if (event->mimeData()->hasFormat("application/x-dnditemdata")) {
        //aceptamos que se mueva los elementos para eliminar los de la clase Run
        event->setDropAction(Qt::MoveAction);
        event->accept();
    } else {
        event->ignore();
    }

}

void Script::dragMoveEvent(QDragMoveEvent *event){

    if (event->mimeData()->hasFormat("application/x-dnditemdata")) {
        //aceptamos que se mueva los elementos para eliminar los de la clase Run
        event->setDropAction(Qt::MoveAction);
        event->accept();
    } else {
        event->ignore();
    }

}

void Script::mousePressEvent(QMouseEvent *event){

    BlockMain *child = static_cast<BlockMain*>(childAt(event->pos()));
    if (!child){
        return;
    }

    Str name(childAt(event->pos())->metaObject()->className());
    if(name == "QTextEdit"){
        return;
    }

    //Guardamos la posicion del posible elemento del posible drag
    this->setElemClick(this->children().indexOf(childAt(event->pos())));

    QPixmap pixmap = *child->pixmap();

    QByteArray itemData;
    QDataStream dataStream(&itemData, QIODevice::WriteOnly);
    dataStream << pixmap << QPoint(event->pos() - child->pos());

    QMimeData *mimeData = new QMimeData;
    mimeData->setData("application/x-dnditemdata", itemData);

    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeData);
    drag->setPixmap(pixmap);
    drag->setHotSpot(event->pos() - child->pos());

    QPixmap tempPixmap = pixmap;
    QPainter painter;
    painter.begin(&tempPixmap);
    painter.fillRect(pixmap.rect(), QColor(colorFOND, colorFOND, colorFOND, colorFOND));
    painter.end();

    child->setPixmap(tempPixmap);

    if (drag->exec(Qt::CopyAction | Qt::MoveAction, Qt::CopyAction) == Qt::MoveAction) {
        child->close();
    } else {
        child->show();
        child->setPixmap(pixmap);
    }
}

void Script::dropEvent(QDropEvent *event){

    //validamos que el evento solo se realice con elementos de la clase Run
    if (event->mimeData()->hasFormat("application/x-dnditemdata") and typeid(*event->source()) == typeid(Run)) {
        QByteArray itemData = event->mimeData()->data("application/x-dnditemdata");
        QDataStream dataStream(&itemData, QIODevice::ReadOnly);

        QPixmap pixmap;
        QPoint offset;
        dataStream >> pixmap >> offset;

        event->setDropAction(Qt::MoveAction);
        event->accept();

    } else {
        event->ignore();
    }

}
