#include "textvalue.h"
#include "newtype.h"

TextValue::TextValue(QWidget *parent): QTextEdit(parent){

    //parametros iniciales de la clase
    this->setReadOnly(false);
    this->setCursor(Qt::IBeamCursor);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setFixedSize(widthBOX, heightBOX);
    this->setEnabled(true);

}

TextValue::~TextValue(){

}

