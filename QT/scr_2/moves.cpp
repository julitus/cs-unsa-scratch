#include "moves.h"

Moves::Moves(Action *action, QWidget *parent): Script(action, parent){

    //Tamaño minimo del contenedor
    this->setMinimumSize(200, 400);
    this->setAcceptDrops(true);

    //Instanciamos los bloques de movimiento
    this->step = new MoveStep(this->getAction(), this);
    this->step->move(xBLOCK, yBLOCK);
    this->turnLeft = new MoveTurnLeft(this->getAction(), this);
    this->turnLeft->move(xBLOCK, yBLOCK + 25);
    this->turnRight = new MoveTurnRight(this->getAction(), this);
    this->turnRight->move(xBLOCK, yBLOCK + 50);
    this->goToX = new MoveGoToX(this->getAction(), this);
    this->goToX->move(xBLOCK, yBLOCK + 75);
    this->goToY = new MoveGoToY(this->getAction(), this);
    this->goToY->move(xBLOCK, yBLOCK + 100);
    this->editAngle = new MoveEditAngle(this->getAction(), this);
    this->editAngle->move(xBLOCK, yBLOCK + 125);
    this->say = new LookSay(this->getAction(), this);
    this->say->move(xBLOCK, yBLOCK + 150);
    this->think = new LookThink(this->getAction(), this);
    this->think->move(xBLOCK, yBLOCK + 175);
    this->show = new LookShow(this->getAction(), this);
    this->show->move(xBLOCK, yBLOCK + 200);
    this->hide = new LookHide(this->getAction(), this);
    this->hide->move(xBLOCK, yBLOCK + 225);
    this->draw = new LookDraw(this->getAction(), this);
    this->draw->move(xBLOCK, yBLOCK + 250);
    this->increaseStep = new MoveIncreaseStep(this->getAction(), this);
    this->increaseStep->move(xBLOCK, yBLOCK + 275);

}

Moves::~Moves(){

}
