#include "exestart.h"
#include "textvalue.h"

ExeStart::ExeStart(): BlockMain(){

    this->type = EXEC;
    this->name = "exestart";
    this->setPixmap(QPixmap(":/img/flag.png"));
    this->show();

}

ExeStart::ExeStart(Action *action, QWidget *parent): BlockMain(action, parent){

    this->type = EXEC;
    this->name = "exestart";
    this->setPixmap(QPixmap(":/img/start.png"));
    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);

}

ExeStart::~ExeStart(){

}

void ExeStart::iterar(QMap<PosE, BlockMain *> *listMap){

    PosE cont = INI;
    bool save = false;
    QMap<PosE, BlockMain* > *newMapa = new QMap<PosE, BlockMain* >;
    PosE nFor = INI;
    for(QMap<PosE, BlockMain* >::iterator it = listMap->begin(); it != listMap->end(); ++it){
        if(!save){
            if(it.value()->getType() == BUCL){
                save = true;
                nFor = static_cast<TextValue*>(it.value()->buddy())->toPlainText().toInt();
            }
            else if(it.value()->getType()){
                it.value()->eventAction();
                this->getAction()->delay(DELAY);
            }
        }else{
            if(it.value()->getType() == BUCL){
                cont++;
            }else if(it.value()->getType() == END){
                cont--;
            }
            if(cont != -1){
                (*newMapa)[it.key()] = static_cast<BlockMain*>(it.value());
            }else{
                while(nFor--){
                    this->iterar(newMapa);
                }
                cont = INI;
                save = false;
                nFor = INI;
                newMapa->clear();
            }
        }

    }

}

void ExeStart::eventAction(){

    this->enabledRun();
    this->iterar(this->getListMap());
    this->enabledRun();
    this->getAction()->defaultParams();

}

ExeStart *ExeStart::copy(QWidget *parent){

    return new ExeStart(this->getAction(), parent);

}

void ExeStart::enabledRun(){

    if(!this->getListMap()->isEmpty()){
        if(this->getListMap()->first()->parentWidget()->isEnabled()){
            this->getListMap()->first()->parentWidget()->setDisabled(true);
        }else{
            this->getListMap()->first()->parentWidget()->setEnabled(true);
        }
    }

}
