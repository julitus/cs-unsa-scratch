#include "cat.h"

Cat::Cat(QGraphicsItem *parent): QGraphicsPixmapItem(parent){

    this->setPixmap(QPixmap(":/img/felixCat.png"));
    this->width = this->boundingRect().width();
    this->height = this->boundingRect().height();
    this->setTransformOriginPoint(this->width/2, this->height/2);
    this->setPos(-this->boundingRect().width()/2, -this->boundingRect().height()/2);
    this->xAns = this->transformOriginPoint().x();
    this->yAns = this->transformOriginPoint().y();
    this->setFlag(QGraphicsItem::ItemIsMovable);

}

Cat::~Cat(){

}

PosE Cat::getWidth(){

    return this->width;

}

PosE Cat::getHeight(){

    return this->height;

}

conv Cat::getXAns(){

    return this->xAns;

}

void Cat::setXAns(conv value){

    this->xAns = value;

}

conv Cat::getYAns(){

    return this->yAns;

}

void Cat::setYAns(conv value){

    this->yAns = value;

}
