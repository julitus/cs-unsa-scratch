#include "lookdraw.h"

LookDraw::LookDraw(Action *action, QWidget *parent): BlockMain(action, parent){

    //Ingresamos la imagen del bloque y la posicion
    this->type = MOTI;
    this->name = "lookdraw";
    this->setPixmap(QPixmap(":/img/paint.png"));

    //mostramos el bloque
    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);
}

void LookDraw::eventAction(){

    this->getAction()->drawChange(true);

}

LookDraw *LookDraw::copy(QWidget *parent){

    return new LookDraw(this->getAction(), parent);

}

LookDraw::~LookDraw()
{

}
