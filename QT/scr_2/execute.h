#ifndef EXECUTE_H
#define EXECUTE_H

#include "script.h"
#include "exestart.h"

class Execute: public Script{

public:
    Execute(Action *action, QWidget *parent = 0);
    void setListMap(QMap<PosE, BlockMain *> &listMap);
    virtual ~Execute();

private:
    ExeStart *start;

};

#endif // EXECUTE_H
