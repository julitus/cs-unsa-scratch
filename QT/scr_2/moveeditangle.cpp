#include "moveeditangle.h"
#include "textvalue.h"

MoveEditAngle::MoveEditAngle(Action *action, QWidget *parent): BlockMain(action, parent)
{
    this->type = MOTI;
    this->name = "moveeditangle";
    this->setPixmap(QPixmap(":/img/angle.png"));

    TextValue *text = new TextValue(this);
    text->move(100, 3);
    text->setText("90");
    this->setBuddy(text);

    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);

}

MoveEditAngle::~MoveEditAngle()
{

}

void MoveEditAngle::eventAction(){

    this->getAction()->editAngle(static_cast<TextValue*>(this->buddy())->toPlainText().toDouble());

}

MoveEditAngle *MoveEditAngle::copy(QWidget *parent){

    return new MoveEditAngle(this->getAction(), parent);

}
