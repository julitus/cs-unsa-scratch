#include "scratch.h"
#include <QHBoxLayout>
#include <QVBoxLayout>

Scratch::Scratch(Cat *cat, QWidget *parent): QFrame(parent){

    //Tamaño minimo del bloque
    this->setMinimumSize(300, 400);

    //Instanciamos la vista y los elementos de la barra
    this->barPos = new View(cat);
    this->start = new ExeStart();
    this->view = new View(cat, this->barPos->getPosition());

    //creamos la barra y agregamos los elementos
    QFrame *bar = new QFrame(this);
    bar->setFixedHeight(30);
    bar->setMinimumWidth(290);
    bar->setFrameStyle(QFrame::Sunken);

    QHBoxLayout *hLayout = new QHBoxLayout;
    hLayout->setMargin(0);
    hLayout->addWidget(barPos);
    hLayout->addWidget(start);
    bar->setLayout(hLayout);

    //agregamos la barra y la vista para mostrarse en esta clase
    QVBoxLayout *vLayout = new QVBoxLayout;
    vLayout->addWidget(bar);
    vLayout->addWidget(view);

    this->setLayout(vLayout);

}

void Scratch::setListMap(QMap<PosE, BlockMain *> &listMap){

    this->start->setListMap(listMap);

}

Scratch::~Scratch(){

}

Position *Scratch::getPosition(){

    return this->view->getPosition();

}

void Scratch::setAction(Action *action){

    this->start->setAction(action);

}
