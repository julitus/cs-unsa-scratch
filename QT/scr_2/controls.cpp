#include "controls.h"

Controls::Controls(Action *action, QWidget *parent): Script(action, parent){

    //Tamaño minimo del contenedor
    this->setMinimumSize(200, 400);
    this->setAcceptDrops(true);

    //Instanciamos los bloques de movimiento
    this->wait = new LookWait(this->getAction(), this);
    this->wait->move(xBLOCK, yBLOCK);
    this->controlFor = new ControlFor(this->getAction(), this);
    this->controlFor->move(xBLOCK, yBLOCK + 25);
    this->controlEnd = new ControlEnd(this->getAction(), this);
    this->controlEnd->move(xBLOCK, yBLOCK + 50);

}

Controls::~Controls(){

}
