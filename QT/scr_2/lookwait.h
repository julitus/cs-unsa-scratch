#ifndef LOOKWAIT_H
#define LOOKWAIT_H

#include "blockmain.h"

class LookWait : public BlockMain{

public:
     LookWait(Action *action, QWidget *parent = 0);
     void eventAction();
     LookWait *copy(QWidget *parent = 0);
     virtual ~LookWait();

};

#endif // LOOKWAIT_H
