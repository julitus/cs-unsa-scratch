#ifndef VIEW_H
#define VIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include "cat.h"
#include "position.h"

class View : public QGraphicsView{

public:
    View(Cat *cat, Position *pos);
    View(Cat *cat);
    Position *getPosition();
    virtual ~View();

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

private:
    QGraphicsScene *scene;
    Cat *cat;
    Position *pos;
    bool dragItem;

};

#endif // VIEW_H
