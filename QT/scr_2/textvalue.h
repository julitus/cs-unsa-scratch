#ifndef TEXTVALUE_H
#define TEXTVALUE_H

#include <QTextBrowser>

//clase encargada del texto editable de los bloques
class TextValue : public QTextEdit{

public:
    TextValue(QWidget *parent = 0);
    virtual ~TextValue();

};

#endif // TEXTVALUE_H
