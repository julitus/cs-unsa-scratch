#include "moveincreasestep.h"
#include "textvalue.h"

MoveIncreaseStep::MoveIncreaseStep(Action *action, QWidget *parent): BlockMain(action, parent){

    //Ingresamos la imagen del bloque y la posicion
    this->type = MOTI;
    this->name = "moveincreasestep";
    this->setPixmap(QPixmap(":/img/increasestep.png"));

    //Instanciamos el campo de texto y lo agregamos al bloque
    TextValue *text = new TextValue(this);
    text->move(80, 3);
    text->setText("0");
    this->setBuddy(text);

    //mostramos el bloque
    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);

}

MoveIncreaseStep *MoveIncreaseStep::copy(QWidget *parent){

    return new MoveIncreaseStep(this->getAction(), parent);

}

void MoveIncreaseStep::eventAction(){

    this->getAction()->increaseStep(static_cast<TextValue*>(this->buddy())->toPlainText().toDouble());

}

MoveIncreaseStep::~MoveIncreaseStep(){

}

