#ifndef MOVEINCREASESTEP_H
#define MOVEINCREASESTEP_H

#include "blockmain.h"

class MoveIncreaseStep : public BlockMain{

public:
    MoveIncreaseStep(Action *action, QWidget *parent = 0);
    void eventAction();
    MoveIncreaseStep *copy(QWidget *parent = 0);
    ~MoveIncreaseStep();

};

#endif // MOVEINCREASESTEP_H
