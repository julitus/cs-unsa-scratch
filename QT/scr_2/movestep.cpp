#include "movestep.h"
#include "textvalue.h"
#include <QDebug>

MoveStep::MoveStep(Action *action, QWidget *parent): BlockMain(action, parent){

    //Ingresamos la imagen del bloque y la posicion
    this->type = MOTI;
    this->name = "movestep";
    this->setPixmap(QPixmap(":/img/movestep.png"));

    //Instanciamos el campo de texto y lo agregamos al bloque
    TextValue *text = new TextValue(this);
    text->move(80, 3);
    text->setText("10");
    this->setBuddy(text);

    //mostramos el bloque
    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);

}

MoveStep::~MoveStep(){

}

void MoveStep::eventAction(){

    this->getAction()->moveStep(static_cast<TextValue*>(this->buddy())->toPlainText().toDouble());

}

MoveStep *MoveStep::copy(QWidget *parent){

    return new MoveStep(this->getAction(), parent);

}
