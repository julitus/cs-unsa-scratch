#include "view.h"
#include "functemplates.h"

View::View(Cat *cat, Position *pos){

    //Creamos la escena
    this->scene = new QGraphicsScene();
    this->scene->setSceneRect(sceneX, sceneY, sceneWIDTH, sceneHEIGHT);
    this->dragItem = false;

    //Instanciamos al gato y los detalles de posicion y lo agregamos a la escena
    this->cat = cat;
    this->pos = pos;
    this->scene->addItem(this->cat);
    this->pos->showPosition();

    QPalette palette;
    palette.setColor(this->backgroundRole(), QColor(250, 255, 100));

    //seteamos los valores de la clase View
    this->setPalette(palette);
    this->setScene(scene);
    this->setMinimumSize(300, 400);
    this->setContentsMargins(INI, INI, INI, INI);
    this->setDragMode(QGraphicsView::ScrollHandDrag);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->show();

}

View::View(Cat *cat): QGraphicsView(){

    //Creamos la escena
    this->scene = new QGraphicsScene();
    this->scene->setSceneRect(INI, INI, 260, 30);
    this->dragItem = false;

    //Instanciamos al gato y los detalles de posicion y lo agregamos a la escena
    this->cat = cat;
    this->pos = new Position(cat);
    this->scene->addItem(this->pos);

    QPalette palette;
    palette.setColor(this->backgroundRole(), QColor(250, 255, 220));

    //seteamos los valores de la clase View
    this->setPalette(palette);
    this->setScene(scene);
    this->setFixedHeight(30);
    this->setMinimumWidth(260);
    this->setContentsMargins(INI, INI, INI, INI);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->show();

}

Position *View::getPosition(){

    return this->pos;

}

View::~View(){

}

void View::mousePressEvent(QMouseEvent *event){

    QGraphicsPixmapItem *child = static_cast<QGraphicsPixmapItem*>(this->itemAt(event->pos()));
    if (child){
        QGraphicsView::mousePressEvent(event);
        this->dragItem = true;
    }

}

void View::mouseReleaseEvent(QMouseEvent *event){

    QGraphicsPixmapItem *child = static_cast<QGraphicsPixmapItem*>(this->itemAt(event->pos()));
    if (child){
        this->pos->showPosition();
        QGraphicsView::mouseReleaseEvent(event);
        this->dragItem = false;
    }
}

void View::mouseMoveEvent(QMouseEvent *event){

    if(this->dragItem){
        this->pos->showPosition();
        QGraphicsView::mouseMoveEvent(event);
    }

}
