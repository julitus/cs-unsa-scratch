#ifndef CREATOR_H
#define CREATOR_H

#include "cat.h"
#include "events.h"
#include "action.h"
#include "run.h"
#include "view.h"
#include "scratch.h"

class Creator: public QWidget{

public:
    Creator(QWidget * parent = 0);
    virtual ~Creator();

private:
    Cat *cat;
    Action *action;
    Scratch *scratch;
    Events *events;
    Run *run;

};

#endif // CREATOR_H
