#ifndef LOOKTHINK_H
#define LOOKTHINK_H

#include "blockmain.h"

class LookThink : public BlockMain{

public:
    LookThink(Action *action, QWidget *parent = 0);
    void eventAction();
    LookThink *copy(QWidget *parent = 0);
    virtual ~LookThink();

};

#endif // LOOKTHINK_H
