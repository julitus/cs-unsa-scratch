#ifndef EXESTART_H
#define EXESTART_H


#include "blockmain.h"

class ExeStart: public BlockMain{

public:
    //constructor que crea un bloque en forma de bandera
    ExeStart();
    //constructor que crea el bloque de ejecucion
    ExeStart(Action* action, QWidget *parent = 0);
    void eventAction();
    ExeStart *copy(QWidget *parent = 0);
    virtual ~ExeStart();

private:
    void iterar(QMap<PosE, BlockMain *> *listMap);
    void enabledRun();

};
#endif // EXESTART_H
