#include "creator.h"
#include <QHBoxLayout>
#include <QMap>

Creator::Creator(QWidget * parent): QWidget(parent){

    //Damos un alto y ancho minimo
    this->setMaximumSize(1000, 600);

    //Creamos el gato que interactuara con la clase Action
    this->cat = new Cat();

    //Instanciamos las clases necesarias para cada bloque del Scratch
    this->scratch = new Scratch(cat);
    this->run = new Run(this);

    //Instanciamos la clase que hara la conexion entre los bloques Scratch, Events y Run
    this->action = new Action(cat, this->scratch->getPosition());
    this->events = new Events(run, action, this);
    this->scratch->setAction(this->action);

    //Creamos el mapa que guardara la lista de bloques en Run
    //Event guarda la misma lista para ejecutarla en un bloque especial
    QMap<PosE, BlockMain*> listMap;
    listMap.clear();
    this->run->setListMap(listMap);
    this->events->setListMap(listMap);
    this->scratch->setListMap(listMap);

    //Ingresamo cada instancia dentro del contenedor
    QHBoxLayout *hLayout = new QHBoxLayout;
    hLayout->addWidget(scratch);
    hLayout->addWidget(events);
    hLayout->addWidget(run);

    //Agregamos el contenedor a esta clase
    this->setLayout(hLayout);

}

Creator::~Creator(){

}

