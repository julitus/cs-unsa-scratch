/**
  *@file action.cpp
  * @name Acciones de bloques
  */

#include "action.h"
#include "math.h"
#include <QDebug>
/**
 * @brief Action::Action
 * @param cat
 * @param position
 */
Action::Action(Cat *cat, Position *position){

    this->cat = cat;
    this->position = position;
    this->draw = false;
    this->upStep = INI;

}

Cat *Action::getCat(){

    return this->cat;

}

void Action::setCat(Cat *cat){

    this->cat = cat;

}

//mover x pasos
void Action::moveStep(conv x){

    conv angle = this->cat->rotation();
    this->cat->setPos(this->cat->x() + (this->upStep + x) * qCos(qDegreesToRadians(angle)),
                      this->cat->y() + (this->upStep + x) * qSin(qDegreesToRadians(angle)));
    this->drawLine();
    this->position->showPosition();

}

//girar g grados a la izquierda
void Action::turnLeft(conv g){

    this->cat->setRotation(this->cat->rotation() - g);

}

//girar g grados a la derecha
void Action::turnRight(conv g){

    this->cat->setRotation(this->cat->rotation() + g);

}

void Action::editAngle(conv g){

    this->cat->setRotation(-g);

}

//ir a la posicion x
void Action::goTo_x(conv x){

    this->cat->setX(-this->cat->getWidth()/2 + x);
    this->drawLine();
    this->position->showPosition();

}

//ir a la posicion y
void Action::goTo_y(conv y){

    this->cat->setY(-this->cat->getHeight()/2 - y);
    this->drawLine();
    this->position->showPosition();

}

void Action::sayWord(dataText word){

    this->createMsj(":/img/say.png", word, this->cat->x() + 0.9*this->cat->getWidth(),
                    this->cat->y() - 0.9*this->cat->getHeight());

}

void Action::thinkWord(dataText word){

    this->createMsj(":/img/think.png", word, this->cat->x() - 0.9*this->cat->getWidth(),
                this->cat->y() - 0.9*this->cat->getHeight());

}

void Action::show(){

    this->cat->show();

}

void Action::hide(){

    this->cat->hide();

}

void Action::wait(PosE sec){

    this->delay(sec * 1000);

}

void Action::delay(PosE time){

    QTime dieTime = QTime::currentTime().addMSecs(time);
    while (QTime::currentTime() < dieTime){
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }

}

Action::~Action(){

}

//muestra un mensaje
void Action::createMsj(dataText path, dataText word, conv x, conv y){

    QImage image(path);

    QPainter* painter = new QPainter(&image);
    painter->setPen(Qt::black);
    painter->setFont(QFont("Arial", sizeWORD));

    painter->drawText(image.rect(), Qt::AlignCenter, word);

    QLabel *msj = new QLabel();
    msj->move(x, y);
    msj->setPixmap(QPixmap::fromImage(image));
    msj->setAlignment(Qt::AlignCenter);
    msj->setAttribute(Qt::WA_DeleteOnClose);

    QGraphicsProxyWidget *proxy = this->cat->scene()->addWidget(msj);
    this->delay(delayMSJ);
    this->cat->scene()->removeItem(proxy);

    msj->close();
    painter->~QPainter();

}

void Action::drawChange(bool change){

    this->draw = change;

}

void Action::defaultParams(){

    this->removeLine();
    this->drawChange(false);
    this->upStep = INI;

}

void Action::drawLine(){

    if(this->draw){
        conv x = this->cat->x() + this->cat->getWidth()/2;
        conv y = this->cat->y() + this->cat->getHeight()/2;
        QGraphicsLineItem *line = this->cat->scene()->addLine(this->cat->getXAns(),
                                                              this->cat->getYAns(), x, y, QPen(Qt::red, sizeCOLOR));
        this->lines.push_back(line);
    }

}

void Action::removeLine(){

    while(!this->lines.empty()){
        this->cat->scene()->removeItem(this->lines.back());
        this->lines.pop_back();
    }

}

void Action::increaseStep(conv x){

    this->upStep = this->upStep + x;

}
