#ifndef TOOLBAR_H
#define TOOLBAR_H

#include <QToolBar>
#include "newtype.h"
#include "blockmain.h"
#include "moves.h"
#include "execute.h"
#include "lookwait.h"
#include "controls.h"
#include "run.h"
#include <map>
#include <iostream>

class ToolBar: public QToolBar{

    Q_OBJECT

public:
    ToolBar(Run *run, Action * action);

private slots:
    void open();
    bool saveAs();
    bool clean();

private:
    Run *run;
    Action * action;
    void addVector(std::vector<Str> &words, Str line);
    void loadFile(dataText fileName);
    bool saveFile(dataText fileName);
    void cleanAll();

    typedef BlockMain* (ToolBar::*PM)(); //definimos el puntero al metodo con el nombre PM

    std::map<Str, PM> mapBlock;
    void createMap();

    //Metodos que crean bloques BlockMain
    BlockMain *createMoveStep(){ return new MoveStep(action, run); }
    BlockMain *createMoveGoToX(){ return new MoveGoToX(action, run); }
    BlockMain *createMoveTurnLeft(){ return new MoveTurnLeft(action, run); }
    BlockMain *createMoveTurnRight(){ return new MoveTurnRight(action, run); }
    BlockMain *createMoveGoToY(){ return new MoveGoToY(action, run); }
    BlockMain *createMoveIncreaseStep(){ return new MoveIncreaseStep(action, run); }
    BlockMain *createMoveEditAngle(){ return new MoveEditAngle(action, run); }
    BlockMain *createLookDraw(){ return new LookDraw(action, run); }
    BlockMain *createLookHide(){ return new LookHide(action, run); }
    BlockMain *createLookWait(){ return new LookWait(action, run); }
    BlockMain *createLookSay(){ return new LookSay(action, run); }
    BlockMain *createLookShow(){ return new LookShow(action, run); }
    BlockMain *createLookThink(){ return new LookThink(action, run); }
    BlockMain *createControlFor(){ return new ControlFor(action, run); }
    BlockMain *createControlEnd(){ return new ControlEnd(action, run); }
    BlockMain *createExeStart(){ return new ExeStart(action, run); }

};

#endif // TOOLBAR_H
