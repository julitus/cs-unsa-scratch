#include "run.h"
#include "scratch.h"
#include "blockmain.h"
#include "textvalue.h"
#include "newtype.h"
#include <typeinfo>

Run::Run(QWidget *parent): Drags(parent){

    //creamos los colores para este bloque
    QPalette palette;
    palette.setColor(backgroundRole(),QColor(250, 255, 221));

    //ingresamos los colores para la clase y parametros iniciales
    this->setPalette(palette);
    this->setFrameStyle(QFrame::Sunken | QFrame::StyledPanel);
    this->setFixedWidth(300);
    this->setMinimumHeight(400);
    this->setAcceptDrops(true);

}

Run::~Run(){

}

void Run::enclose(BlockMain *block){

    PosE maxRange = rangeBLOCK;
    foreach(BlockMain *tb, *this->getListMap()){
        if(block->y() >= tb->y() and block->y() <= tb->y() + tb->height() + maxRange){
            block->move(tb->x(), tb->y() + tb->height() - minBLOCK);
            return;
        }else if(block->y() <= tb->y() and block->y() + tb->height() > tb->y() - maxRange){
            block->move(tb->x(), tb->y() - block->height() + minBLOCK);
            return;
        }
    }

}

void Run::insertBlockMap(BlockMain *block){

    (*this->listMap)[block->y()] = block;

}

void Run::removeBlockMap(PosE temp){

    this->listMap->remove(temp);

}

QMap<PosE, BlockMain *> *Run::getListMap(){

    return this->listMap;

}

void Run::setListMap(QMap<PosE, BlockMain *> &listMap){

    this->listMap = &listMap;

}

void Run::mousePressEvent(QMouseEvent *event){

    BlockMain *child = static_cast<BlockMain*>(childAt(event->pos()));
    if (!child){
        return;
    }
    Str name(childAt(event->pos())->metaObject()->className());
    if(name == "QTextEdit"){
        return;
    }

    //Removemos el ultimo bloque que se movio del mapa
    this->removeBlockMap(child->y());

    //Guardamos la posicion del posible elemento del posible drag
    this->setElemClick(this->children().indexOf(childAt(event->pos())));

    QPixmap pixmap = *child->pixmap();

    QByteArray itemData;
    QDataStream dataStream(&itemData, QIODevice::WriteOnly);
    dataStream << pixmap << QPoint(event->pos() - child->pos());

    QMimeData *mimeData = new QMimeData;
    mimeData->setData("application/x-dnditemdata", itemData);

    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeData);
    drag->setPixmap(pixmap);
    drag->setHotSpot(event->pos() - child->pos());

    QPixmap tempPixmap = pixmap;
    QPainter painter;
    painter.begin(&tempPixmap);
    painter.fillRect(pixmap.rect(), QColor(127, 127, 127, 127));
    painter.end();

    child->setPixmap(tempPixmap);

    if (drag->exec(Qt::CopyAction | Qt::MoveAction, Qt::CopyAction) == Qt::MoveAction) {
        child->close();
    } else {
        child->show();
        child->setPixmap(pixmap);
    }

}

void Run::dropEvent(QDropEvent *event){

    //validamos que el evento solo se realice con elementos diferentes de la clase Scratch
    if (event->mimeData()->hasFormat("application/x-dnditemdata") and typeid(*event->source()) != typeid(Scratch)) {
        QByteArray itemData = event->mimeData()->data("application/x-dnditemdata");
        QDataStream dataStream(&itemData, QIODevice::ReadOnly);

        QPixmap pixmap;
        QPoint offset;
        dataStream >> pixmap >> offset;

        BlockMain *newBlock = static_cast<BlockMain*>(
                    event->source()->children().value(static_cast<Drags*>(
                                 event->source())->getElemClick()))->copy(this);

        //validamos si el bloque contiene algun QWidget agregado
        if(static_cast<BlockMain*>(
                    event->source()->children().value(static_cast<Drags*>(
                             event->source())->getElemClick()))->buddy()){

            //extraemos el texto
            dataText text = static_cast<TextValue*>(static_cast<BlockMain*>(
                            event->source()->children().value(static_cast<Drags*>(
                                     event->source())->getElemClick()))->buddy())->toPlainText();

            //ingresamos el texto en la copia
            static_cast<QTextEdit*>(newBlock->buddy())->setText(text);

        }

        newBlock->setPixmap(pixmap);
        newBlock->move(event->pos() - offset);
        newBlock->show();
        newBlock->setAttribute(Qt::WA_DeleteOnClose);

        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } else {
            event->acceptProposedAction();
        }

        //encajamos el bloque con los continuos
        this->enclose(static_cast<BlockMain*>(this->children().last()));

        //Insertamos el nuevo bloque del mapa
        this->insertBlockMap(static_cast<BlockMain*>(this->children().last()));

    } else {
        event->ignore();
    }

}

