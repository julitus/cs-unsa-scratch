#ifndef LOOKSHOW_H
#define LOOKSHOW_H

#include "blockmain.h"

class LookShow : public BlockMain{

public:
    LookShow(Action *action, QWidget *parent = 0);
    void eventAction();
    LookShow *copy(QWidget *parent = 0);
    virtual ~LookShow();

};

#endif // LOOKSHOW_H
