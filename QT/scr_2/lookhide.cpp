#include "lookhide.h"

LookHide::LookHide(Action *action, QWidget *parent): BlockMain(action, parent){

    //Ingresamos la imagen del bloque y la posicion
    this->type = MOTI;
    this->name = "lookhide";
    this->setPixmap(QPixmap(":/img/lookhide.png"));

    //mostramos el bloque
    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);
}

void LookHide::eventAction(){

    this->getAction()->hide();

}

LookHide *LookHide::copy(QWidget *parent){

    return new LookHide(this->getAction(), parent);

}

LookHide::~LookHide(){

}

