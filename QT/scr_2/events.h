#ifndef EVENTS_H
#define EVENTS_H

#include <QFrame>
#include "moves.h"
#include "controls.h"
#include "execute.h"
#include "action.h"
#include "toolbar.h"
#include "run.h"

class Events : public QFrame{

public:
    Events(Run *run, Action *action, QWidget *parent = 0);
    void setListMap(QMap<PosE, BlockMain *> &listMap);
    virtual ~Events();

private:
    ToolBar *bar;
    Action *action;
    Moves *move;
    Controls *control;
    Execute *execute;
};

#endif // EVENTS_H
