var indexSectionsWithContent =
{
  0: "abcdefgimnprstv~",
  1: "abcdemprstv",
  2: "abcdefmnprstv",
  3: "abcdegimnprstv~",
  4: "t",
  5: "cdp",
  6: "t",
  7: "bcem"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues"
};

var indexSectionLabels =
{
  0: "Todo",
  1: "Clases",
  2: "Archivos",
  3: "Funciones",
  4: "Variables",
  5: "&apos;typedefs&apos;",
  6: "Enumeraciones",
  7: "Valores de enumeraciones"
};

