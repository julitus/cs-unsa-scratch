#include "controlend.h"

ControlEnd::ControlEnd (Action *action, QWidget *parent): BlockMain(action, parent){

    this->type = END;
    this->name = "controlend";
    this->setPixmap(QPixmap(":/img/end.png"));

    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);

}

ControlEnd::~ControlEnd(){

}

void ControlEnd::eventAction(){

}

ControlEnd *ControlEnd::copy(QWidget *parent){

    return new ControlEnd(this->getAction(), parent);

}
