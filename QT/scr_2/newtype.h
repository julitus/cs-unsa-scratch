#ifndef NEWTYPE
#define NEWTYPE

#include <QtWidgets>

/*constantes*/
const int DELAY = 300;
const int delayMSJ = 1500;
const int xBLOCK = 30;
const int yBLOCK = 30;
const int INI = 0;
const int colorFOND = 127;
const int sizeWORD = 10;
const int sizeCOLOR = 3;
const int rangeBLOCK = 20;
const unsigned int minBLOCK = 4;
const unsigned int widthBOX = 32;
const unsigned int heightBOX = 14;
const int sceneX = -150;
const int sceneY = -200;
const int sceneWIDTH = 300;
const int sceneHEIGHT = 400;

/*variables*/
typedef char ch;
typedef QString dataText;
typedef int PosE;
typedef unsigned int uPosE;
typedef double conv;
typedef std::string Str;
enum typeBlock {EXEC = 0, MOTI = 1, COND = 2, BUCL = 3, END = 4};
/*typedef const int PosBlock = 30;*/
//Estos  son los valores que represetan el tipo de bloques: EXEC(ejecucion), MOTI(bloque de movimiento),COND(bloque condiciones  como el 'if')y END(bloque para finalizar el bloque 'for')

#endif // NEWTYPE

