/**
  *@file controlfro.h
  * @name Bloque del bucle For
  */

#ifndef CONTROLFOR_H
#define CONTROLFOR_H

#include "blockmain.h"
/**
 * @brief The ControlFor class Esta clase sera una de las encargadas de que los movimientos del personaje sean continuos deacuerdo a lo que
 * el usuario desee.
 */

class ControlFor : public BlockMain{

public:
    /**
     * @brief ControlFor
     * @param action
     * @param parent
     */
    ControlFor(Action *action, QWidget *parent = 0);
    virtual ~ControlFor();
    /**
     * @brief eventAction
     */
    void eventAction();
    /**
     * @brief copy
     * @param parent
     * @return
     */
    ControlFor *copy(QWidget *parent = 0);

};

#endif // CONTROLFOR_H
