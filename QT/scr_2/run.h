#ifndef RUN_H
#define RUN_H

#include "drags.h"
#include "blockmain.h"
#include <QMap>

class Run : public Drags{

public:
    Run(QWidget *parent = 0);
    void insertBlockMap(BlockMain *block);
    void removeBlockMap(PosE temp);
    QMap<PosE, BlockMain *> *getListMap();
    void setListMap(QMap<PosE, BlockMain *> &listMap);
    virtual ~Run();

private:
    QMap<PosE, BlockMain *> *listMap;
    void enclose(BlockMain *block);


protected:
    void mousePressEvent(QMouseEvent *event);
    void dropEvent(QDropEvent *event);

};

#endif // RUN_H
