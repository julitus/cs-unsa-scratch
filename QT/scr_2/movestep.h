#ifndef MOVESTEP_H
#define MOVESTEP_H

#include "blockmain.h"

class MoveStep : public BlockMain{

public:
    MoveStep(Action *action, QWidget *parent = 0);
    void eventAction();
    MoveStep *copy(QWidget *parent = 0);
    virtual ~MoveStep();

};

#endif // MOVESTEP_H
