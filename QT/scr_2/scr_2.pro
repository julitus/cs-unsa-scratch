#-------------------------------------------------
#
# Project created by QtCreator 2015-06-20T12:56:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = scr_2
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp \
    scratch.cpp \
    events.cpp \
    run.cpp \
    drags.cpp \
    moves.cpp \
    creator.cpp \
    movestep.cpp \
    moveturnleft.cpp \
    textvalue.cpp \
    blockmain.cpp \
    controls.cpp \
    script.cpp \
    action.cpp \
    execute.cpp \
    view.cpp \
    cat.cpp \
    moveturnright.cpp \
    movegotox.cpp \
    movegotoy.cpp \
    exestart.cpp \
    position.cpp \
    moveeditangle.cpp \
    controlif.cpp \
    controlfor.cpp \
    controlend.cpp \
    looksay.cpp \
    lookthink.cpp \
    lookshow.cpp \
    lookhide.cpp \
    lookwait.cpp \
    lookdraw.cpp \
    moveincreasestep.cpp \
    toolbar.cpp

HEADERS  += \
    scratch.h \
    events.h \
    run.h \
    newtype.h \
    drags.h \
    moves.h \
    creator.h \
    movestep.h \
    moveturnleft.h \
    textvalue.h \
    blockmain.h \
    controls.h \
    script.h \
    action.h \
    execute.h \
    view.h \
    cat.h \
    moveturnright.h \
    movegotox.h \
    movegotoy.h \
    exestart.h \
    position.h \
    moveeditangle.h \
    controlif.h \
    controlfor.h \
    controlend.h \
    functemplates.h \
    looksay.h \
    lookthink.h \
    lookshow.h \
    lookhide.h \
    lookwait.h \
    lookdraw.h \
    moveincreasestep.h \
    toolbar.h

RESOURCES += \
    scratch.qrc

DISTFILES += \
    error.txt \
    anotation.txt
