/**
  *@file controls.h
  * @name -----------------
  */
#ifndef CONTROLS_H
#define CONTROLS_H

#include "script.h"
#include "controlfor.h"
#include "controlend.h"
#include "lookwait.h"
/**
 * @brief The Controls class
 */


class Controls: public Script{

public:
    /**
     * @brief Controls
     * @param action
     * @param parent
     */
    Controls(Action *action, QWidget *parent = 0);
    virtual ~Controls();

private:
    /**
     * @brief wait
     */
    LookWait *wait;
    /**
     * @brief controlFor
     */
    ControlFor *controlFor;
    /**
     * @brief controlEnd
     */
    ControlEnd *controlEnd;

};

#endif // CONTROLS_H
