#include "controlfor.h"
#include "textvalue.h"

ControlFor::ControlFor (Action *action, QWidget *parent): BlockMain(action, parent){

    this->type = BUCL;
    this->name = "controlfor";
    this->setPixmap(QPixmap(":/img/for.png"));

    TextValue *text = new TextValue(this);

    text->move(90, 3);
    text->setText("5");
    this->setBuddy(text);

    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);

}

ControlFor::~ControlFor(){

}

void ControlFor::eventAction(){

}

ControlFor *ControlFor::copy(QWidget *parent){

    return new ControlFor(this->getAction(), parent);

}
