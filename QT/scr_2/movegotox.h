#ifndef MOVEGOTOX_H
#define MOVEGOTOX_H

#include "blockmain.h"

class MoveGoToX : public BlockMain{

public:
    MoveGoToX(Action *action, QWidget *parent = 0);
    void eventAction();
    MoveGoToX *copy(QWidget *parent = 0);
    virtual ~MoveGoToX();

};

#endif // MOVEGOTOX_H
